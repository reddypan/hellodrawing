//
//  MyView.swift
//  Hello, Drawing
//
//  Created by Pannala,Vishal Reddy on 4/9/19.
//  Copyright © 2019 Northwest. All rights reserved.
//

import UIKit

class MyView: UIView {

    var points: [[CGPoint]] = []
    var colors: [UIColor] = []

    override func draw(_ rect: CGRect) {
        for i in 0..<points.count {
            if points[i].count == 0{
                return
            }
            let path = UIBezierPath()
            colors[i].setStroke()
            path.move(to: points[i][0])
            for pt in points[i] {
                path.addLine(to: pt)
            }
            path.stroke()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        points.append([(touch?.location(in: self))!])
        colors.append(UIColor(displayP3Red: CGFloat.random(in: 0.0...1.0), green: CGFloat.random(in: 0.0...1.0), blue: CGFloat.random(in: 0.0...1.0), alpha: CGFloat.random(in: 0.0...1.0)))
        print("Began: \(String(describing: touch?.location(in: self)))")
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        points[points.count-1].append((touch?.location(in: self))!)
        print("Moved: \(String(describing: touch?.location(in: self)))")
        setNeedsDisplay()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        points[points.count-1].append((touch?.location(in: self))!)
        print("Ended: \(String(describing: touch?.location(in: self)))")
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        points.append([(touch?.location(in: self))!])
        print("Cancelled: \(String(describing: touch?.location(in: self)))")
    }

}
